/*
Создать таблицу, при клике на ячейки которой они будут менять цвет.

    Технические требования:

    Создать поле 30*30 из белых клеточек с помощью элемента <table>.
При клике на белую клеточку она должна менять цвет на черный. При клике на черную клеточку она должна менять цвет обратно на белый.
    Сама таблица должна быть не вставлена в исходный HTML код, а сгенерирована и добавлена в DOM страницы с помощью Javascript.
    На странице должен быть только один обработчик события click, который будет повешен на всю таблицу. События всплывают от элемента вверх по DOM дереву, их все можно ловить с помощью одного обработчика событий на таблице, и в нем определять, на какую из ячеек нажал пользователь.
    При клике на любое место документа вне таблицы, все цвета клеточек должны поменяться на противоположные (подсказка: нужно поставить Event Listener на <body>).
Чтобы поменять цвета всех клеточек сразу, не нужно обходить их в цикле. Если помечать нажатые клетки определенным классом, то перекрасить их все одновременно можно одним действием - поменяв класс на самой таблице.
*/
function TableConstructor(cellsX, rowsY, tdWidth, tdHeight, tdColor, tdColorChanged) {
    let styleCheck = document.getElementsByTagName('style')[0];
    let style = false;
    if (!styleCheck) {
        style = document.createElement('style');
        style.type = 'text/css';
    }
    else {
        style = document.getElementsByTagName('style')[0];
    }

    style.innerHTML += `.table { border-collapse: collapse; margin: 0 auto }    
                   .table__cell { width: ${tdWidth}px; height: ${tdHeight}px; border: 1px solid #555555; background-color: ${tdColor};}    
                   .table__cell_colorized { background-color: ${tdColorChanged}; }
                   .table_inverted .table__cell {background-color: ${tdColorChanged};}
                   .table_inverted .table__cell_colorized { background-color: ${tdColor}; }`;
    if (!styleCheck) {
        document.getElementsByTagName('head')[0].appendChild(style);
    }
    let string1 = `<td class="table__cell table__cell_white"></td>`
    let tdString = new Array(cellsX + 1).join(string1);
    let trString = new Array(rowsY + 1).join(`<tr>${tdString}</tr>`);
    this.table = `<table class="table"><tbody>${trString}</tbody></table>`
}

TableConstructor.prototype.insertInDom = function () {
    document.body.innerHTML = `${this.table}`;
}
TableConstructor.prototype.colorized = function () {
    let tableDom = document.getElementsByClassName('table')[0];
    tableDom.addEventListener('click', function () {
        let selectCell = event.target;
        selectCell.classList.toggle('table__cell_colorized');
    });

}
TableConstructor.prototype.invert = function () {
    let tableDom = document.getElementsByClassName('table')[0];
    document.getElementsByTagName('body')[0].addEventListener('click', function () {
        let target = event.target;
        if (target.closest('.table')) {
            return true;
        }
        tableDom.classList.toggle('table_inverted');

    });

}
let table = new TableConstructor(30, 30, 20, 20, '#fff', '#000');
table.insertInDom();
table.colorized();
table.invert();

