class Hamburger {

    constructor(size, stuffing) {
        try {
            if (arguments.length === 0) {
                throw new HamburgerException("no size given");
            }
            if (arguments.length === 1) {
                throw new HamburgerException("no stuffing given");
            }
            if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
                throw new HamburgerException("invalid size parameter");
            }
            if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD) {
                throw new HamburgerException("invalid stuffing");
            }
            this._size = JSON.parse(size);
            this._stuffing = JSON.parse(stuffing);
            this._toppings = [];

        } catch (e) {
            console.log(e.message);
        }
    }
    static get SIZE_SMALL() {
        return '{"name":"SIZE_SMALL", "price":50, "calories":20}'
    }
    static get SIZE_LARGE() {
        return '{"name":"STUFFING_CHEESE", "price":10, "calories":20}'
    }
    static get STUFFING_CHEESE() {
        return '{"name":"STUFFING_CHEESE", "price":10, "calories":20}'
    }
    static get STUFFING_SALAD() {
        return '{"name":"STUFFING_SALAD", "price":20, "calories":5}'
    }
    static get STUFFING_POTATO() {
        return '{"name":"STUFFING_POTATO", "price":15, "calories":10}'
    }
    static get TOPPING_MAYO() {
        return '{"name":"TOPPING_MAYO", "price":20, "calories":5}'
    }
    static get TOPPING_SPICE() {
        return '{"name":"TOPPING_SPICE", "price":15, "calories":0}'
    }

    set addTopping (topping) {
        try {
            let toppingObj = JSON.parse(topping);
            if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE) {
                throw new HamburgerException("invalid topping " + toppingObj.name);
            }
            else if (this._toppings.indexOf(topping) >= 0) {
                throw new HamburgerException("duplicate topping " + toppingObj.name);
            }
            else {
                this._toppings.push(topping);
            }

        } catch (e) {
            console.log(e.name, e.message);
        }
    }
    removeTopping (topping) {
        try {
            let toppingObj = JSON.parse(topping);
            if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE) {
                throw new HamburgerException("invalid topping " + toppingObj.name);
            }
            else if (this._toppings.indexOf(topping) < 0) {
                throw new HamburgerException("the topping " + toppingObj.name + " is absent in this hamburger");
            }
            else {
                this._toppings.splice(this._toppings.indexOf(topping), 1);
            }
        } catch (e) {
            console.log(e.name, e.message);
        }
    }
    get getToppings () {
        return this._toppings;
    }
    get getSize () {
        return this._size.name;
    }
    get getStuffing () {
        return this._stuffing.name;
    }
    calculatePrice () {
        let price = this._size['price'] + this._stuffing['price'];
        this._toppings.forEach(function (elem) {
            price += JSON.parse(elem)['price'];
        });
        return price;
    };
    calculateCalories () {
        let calories = this._size['calories'] + this._stuffing['calories'];
        this._toppings.forEach(function (elem) {
            calories += JSON.parse(elem)['calories'];
        });
        return calories;
    };
}

// Наследуя от стандартного класса Error можно выводить не только переопределенное нами свойство message, но и другие свойства класса Error
class HamburgerException extends Error {
    constructor (message){
        super();
    this.message = message;
    }
}

let burg = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
burg.addTopping = Hamburger.TOPPING_SPICE;
burg.addTopping = Hamburger.TOPPING_SPICE;
burg.addTopping = Hamburger.TOPPING_MAYO;
burg.removeTopping(Hamburger.TOPPING_SPICE);

console.log(burg);


/*
## Задание

Переделать домашнее задание 3, используя синтаксис и возможности стандарта ES6.

    #### Технические требования:
    - Функция-конструктор должна выглядеть следующим образом:
    ```javascript
   class Hamburger {
      ...
   }
   ```
    - Необходимо использовать геттеры-сеттеры и прочие возможности классов в ES6.

    #### Литература:
- [Классы в ES6](https://learn.javascript.ru/es-class)
*/