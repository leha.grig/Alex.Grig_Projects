/*Нарисовать на странице круг используя параметры, которые введет пользователь.

Технические требования:

При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript.
При нажатии на кнопку - вместо нее показать два поля ввода. В первом пользователь сможет ввести диаметр круга в пикселях. Во втором - цвет круга (в любом формате, который понимает CSS - имя цвета, RGB, HEX, HSL).
Под полями ввода должна быть кнопка "Нарисовать". При нажатии - на странице должен появиться круг заданного пользователем диаметра и с заливкой указанного цвета.


Не обязательное задание продвинутой сложности:

При нажатии на кнопку "Нарисовать круг" показывать только одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на каждый - они должны исчезать
У вас может возникнуть желание поставить обработчик события на каждый круг для его исчезновения. Это неэффективно, так делать не нужно. На всю страницу должен быть только один обработчик событий, который будет это делать.*/

const drawButton = document.getElementById('drawButton');


/*drawButton.addEventListener('click', function () {
    document.body.innerHTML = `<input type="text" id="radius" placeholder="enter radius">
   <input type="text" id="color" placeholder="enter color">
   <p><button id="drawCircleBtn">Нарисовать</button></p>`;
    
    const drawCircle = document.getElementById('drawCircleBtn');
    const radiusField = document.getElementById('radius');
    const colorField = document.getElementById('color');   
    
    drawCircle.addEventListener('click', function () {    
    document.body.innerHTML += `<div style = 'display: inline-block;
    width: ${radiusField.value}px;
    height: ${radiusField.value}px;
    background-color: ${colorField.value};
    border-radius: 50%;
    box-sizing: border-box;'></div>`;        
    });
});*/

drawButton.addEventListener('click', function () {
    
    document.body.innerHTML = `<input type="text" id="radius" placeholder="enter radius">
   <p><button id="drawCircleBtn">Нарисовать</button></p>`;

    const drawCircle = document.getElementById('drawCircleBtn'); // можно без этой переменной - обработчик работает и просто по ID - как видно из кода ниже
    const radiusField = document.getElementById('radius');
    
    
    // почему повторное введение и повторный клик не срабатывают???
    drawCircleBtn.addEventListener('click', function () {
        console.log ('Hello');
        document.body.innerHTML += `<div class="container" id="container" style = 'width: ${(radiusField.value)*10}px;'></div>`;
        let container = document.getElementById('container');
        for (let i = 0; i < 100; i++) {
            container.innerHTML += `<div class="circle" style = 'display: inline-block;
    width: ${radiusField.value}px;
    height: ${radiusField.value}px;
    background-color: #${(Math.floor(Math.random()*(256))).toString(16)}${(Math.floor(Math.random()*(256))).toString(16)}${(Math.floor(Math.random()*(256))).toString(16)};
    border-radius: 50%;
    box-sizing: border-box;'></div>`;
        }
        
      /*  
      // попытка сделать исчезание перебором. Неудачная. Почему не работает? - потому что переменную в условии цикла нужно задавать через let, а не через var!!!
      
      let c = document.getElementsByClassName('circle');
        for (var i = 0; i < c.length; i++) {
            console.log(c[i]);
            c[i].addEventListener('click', function () {
                c[i].style.display = 'none'; // почему не работает? почему c[i] тут undefined? 
            });
        }*/
        
        container.addEventListener('click', function (e){
            container.removeChild(e.target);
        })
    });

});
