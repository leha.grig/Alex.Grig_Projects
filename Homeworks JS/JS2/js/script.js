/*Реализовать программу на Javascript, которая будет находить все простые числа в заданном диапазоне.

Технические требования:

Считать с помощью модального окна браузера число, которое введет пользователь.
Вывести в консоли все простые числа от 1 до введенного пользователем числа.
Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


Не обязательное задание продвинутой сложности:

Проверить, что введенное значение является целым числом, и больше единицы. Если данные условия не соблюдаются, повторять вывод окна на экран до тех пор, пока не будет введено целое число больше 1.
Максимально оптимизировать алгоритм, чтобы он выполнялся как можно быстрее для больших чисел.
Считать два числа, m и n. Вывести в консоль все простые числа в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условия валидации, указанные выше, вывести сообщение об ошибке, и спросить оба числа заново.*/
(function () {

    function requestNumbers() {
        let num1 = Number(prompt('Enter the first number'));
        let num2 = Number(prompt('Enter the second number'));
        return [num1, num2];
    }

    let userNumbers = requestNumbers();
    let number1 = userNumbers[0];
    let number2 = userNumbers[1];

    while (!Number.isInteger(number1) || number1 <= 1 || !Number.isInteger(number2) || number2 <= 1) {
        alert('Numbers must be in digital format and greater than 1');
        userNumbers = requestNumbers();
        number1 = userNumbers[0];
        number2 = userNumbers[1];;
    }

    // большее число должно быть вторым:
    if (number1 > number2) {
        number2 = [number1, number1 = number2][0];

        /*
        // то же, но не так красиво:
        let tempNum = number1;
        number1 = number2;
        number2 = tempNum;
        */
    }

    checkiandj: for (i = number1; i <= number2; i++) {
        for (j = 2; j <= Math.sqrt(i); j++) {
            if (Number.isInteger(i / j)) {
                continue checkiandj;
            }
        }
        console.log(i);
    }
})();
