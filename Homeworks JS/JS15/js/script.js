/*Реализовать секундомер с точностью до миллисекунд.

Технические требования:

При запуске программы на экране должно быть табло с минутами, секундами и миллисекундами, а также две кнопки - Start и Clear.
При нажатии на кнопку Start - запускать отсчет секундомера. При этом вместо кнопки Start должна появиться кнопка Pause.
Нажатие кнопки Pause временно останавливает отсчет, до повторного нажатия кнопки Start.
Нажатие кнопки Clear останавливает отсчет (если он был запущен) и обнуляет таймер.


Не обязательное задание продвинутой сложности:

Реализовать таймер не с электронным, а циферблатным вариантом отображения. Миллисекунды должны быть показаны в отдельном маленьком циферблате.
#### Литература:
- [setTimeout и setInterval](https://learn.javascript.ru/settimeout-setinterval)
- [JavaScript: цикличные таймеры с автокоррекцией](https://habr.com/post/212889/)
- [Пример — часы на СSS3 без изображений и JavaScript](https://habr.com/post/171015/)
*/


// переписать функцию как функцию-конструктор объекта!
// функцию setTimeout сделали внешней переменной, с которой работает каждая вложенная функция, чтоб по клику смена стилей срабатывала неограниченное количесвто раз, в том числе через смену классов.
// в коде закоменченном ниже все адекватно работает только с инлайновой сменой стилей, а при смене по классам - только до двух кликов

(function () {

    let min = 0;
    let sec = 0;
    let msec = 0;
    //pauseBtn.style.display = 'none';
    timerField.value = `${min}:${sec}:${msec}`;
    let timeoutID = '';
    startBtn.addEventListener('click', function () {
        const startTime = Date.now();

        // лучше менять стили через смену классов, а не прописывание инлайн-стилей!
        //    pauseBtn.style.display = 'inline-block';
        //    startBtn.style.display = 'none';
        pauseBtn.classList.toggle('hide');
        startBtn.classList.toggle('hide');
        timeoutID = setTimeout(function f() {
            let date = new Date(2018, 0, 0, 0, min, sec, msec);
            date.setMilliseconds(msec + (Date.now() - startTime));

            timerField.value = `${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`;


            timeoutID = setTimeout(f, 4);

        }, 4);
    });

    pauseBtn.addEventListener('click', function () {
        let timeArr = timerField.value.split(':');
        clearTimeout(timeoutID);
        min = Number(timeArr[0]);
        sec = Number(timeArr[1]);
        msec = Number(timeArr[2]);

        pauseBtn.classList.toggle('hide');
        startBtn.classList.toggle('hide');

        //        startBtn.style.display = 'inline-block';
        //        pauseBtn.style.display = 'none';
    });

    clearBtn.addEventListener('click', function () {
        clearTimeout(timeoutID);
        min = 0;
        sec = 0;
        msec = 0;
        timerField.value = `${min}:${sec}:${msec}`;
        startBtn.style.display = 'inline-block';
        pauseBtn.style.display = 'none';
    });

    return false;
})();


/*
(function () {

    let min = 0;
    let sec = 0;
    let msec = 0;
    pauseBtn.style.display = 'none';
    timerField.value = `${min}:${sec}:${msec}`;

    startBtn.addEventListener('click', function () {
        const startTime = Date.now();
        
    // почему, если эти же стили менять добавлением/убиранием класса .hide, через id.classList.toggle('hide'), то кнопки start/pause срабатывают только дважды, а потом перестают работать? (как это исправить - в коде ниже)
        // лучше менять стили через смену классов, а не прописывание инлайн-стилей!
        pauseBtn.style.display = 'inline-block';
        startBtn.style.display = 'none';
        //    pauseBtn.classList.toggle('hide');
        //    startBtn.classList.toggle('hide');
        let timeoutID = setTimeout(function f() {
            let date = new Date(2018, 0, 0, 0, min, sec, msec);
            date.setMilliseconds(msec + (Date.now() - startTime));

            timerField.value = `${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`;

            /!*
                            // второй вариант вывода времени на экран, используя только мс из даты
                            
                            let msec = Date.now() - startTime;
                            let sec = Math.floor(msec / 1000);
                            let minutes = Math.floor(sec / 60);
                            if (msec > 999) {
                                msec = msec % 1000;
                            }
                            if (sec > 59) {
                                sec = 0;
                            }
                    
                            timerForm.timerField.value = `${minutes} : ${sec} : ${msec}`;
                            *!/


            timeoutID = setTimeout(f, 4);

        }, 4);

        pauseBtn.addEventListener('click', function () {
            let timeArr = timerField.value.split(':');
            clearTimeout(timeoutID);
            min = Number(timeArr[0]);
            sec = Number(timeArr[1]);
            msec = Number(timeArr[2]);

            //        pauseBtn.classList.toggle('hide');
            //    startBtn.classList.toggle('hide');

            startBtn.style.display = 'inline-block';
            pauseBtn.style.display = 'none';
        })

        clearBtn.addEventListener('click', function () {
            clearTimeout(timeoutID);
            min = 0;
            sec = 0;
            msec = 0;
            timerField.value = `${min}:${sec}:${msec}`;
            startBtn.style.display = 'inline-block';
            pauseBtn.style.display = 'none';
        })
    })
    return false;
})();*/



