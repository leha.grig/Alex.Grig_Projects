/*Реализовать функцию-конструктор для создания объекта "пользователь".

Технические требования:

Написать функцию createNewUser(), которая будет создавать и возвращать объект "пользователь".
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект со свойствами firstName и lastName.
Добавить в объект метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре.


Не обязательное задание продвинутой сложности:

Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свйоства.*/

function createNewUser(name = prompt('Enter your name', ''), surname = prompt('Enter your surname', '')) {

    Object.defineProperty(this, "firstName", {
        value: name,
        writable: false,
        configurable: true, // если это свойство false, то переопределение firstName сеттером не работает
        
    });

    Object.defineProperty(this, "lastName", {
        value: surname,
        writable: false,
        configurable: true,
    });
    
      
       // благодаря стрелочной функции (не имеет своего this) в аргументах функции Object.defineProperty() this - это объект createNewUser ()
    this.setFirstName = (elem) => {
        Object.defineProperty(this, "firstName", {
            value: elem,            
        });
        
    };

    this.setLastName = (elem) => {
        Object.defineProperty(this, "lastName", {        
            value: elem,
        });
        
    };
    this.getLogin = function () {
        let login = (this.firstName.charAt(0) + this.lastName).toLowerCase();
        return login;
    }
}
let user = new createNewUser();
console.log(obj.firstName);
console.log(obj.getLogin());
