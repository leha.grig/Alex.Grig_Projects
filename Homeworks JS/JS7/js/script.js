/*Реализовать функцию нахождения уникальных объектов в массиве при сравнении с другим массивом, по указанному пользователем свойству.

Технические требования:


Написать функцию excludeBy(), которая в качестве первых двух параметров будет принимать на вход два массива с объектами любого типа, например:
const users = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
}]


В качестве третьего параметра функция должна принимать имя поля, по которому будет проводиться сравнение.


Функция должна вернуть новый массив, который будет включать те объекты из первого массива, значение указанного свойства которых не встречается среди объектов, представленных во втором массиве. Например, вызов функции excludeBy(peopleList, excluded, 'name') должен возвращать массив из тех пользователей peopleList, имя которых (свойство name) не встречается у пользователей, которые находятся в массиве excluded.*/

/*план
1. Создать новый (третий) массив
2. В первом массиве взять первый объект
3. Проверить, есть ли в нем указанное свойство
4. если есть, то перебрать все объекты второго массива и сравнить в них значение указанного свойства со значением выбранного свойства первого объекта первого массива
5. Если совпадений не найдено - скопировать выбранный объект как свойство третьего массива. Если совпадение найдено - перейти на новую итерацию (повторить то же (п.3-5) со следующим объектом первого массива)
6. вернуть третий массив*/

function excludeBy(arr1, arr2, prop) {
    let newList = [];
    arr1.forEach(function (item) {
        if (item.hasOwnProperty(prop)) {
            for (i = 0; i < arr2.length; i++) {
                if (item[prop] == arr2[i][prop]) {
                    return false;
                } else if (i == arr2.length - 1) {
                    newList.push(item);
                }
            }
        }
    })
    return newList;
}


// проверка
const users1 = [{
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        age: 30
},
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
},
    {
        name: "Vasya",
        surname: "Petrov",
        gender: "male",
        age: 24
}];

const users2 = [{
        name: "Petya",
        surname: "Ivanov",
        gender: "male",
        age: 30
},
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
},
    {
        name: "Vasya",
        surname: "Ivanov",
        gender: "male",
        age: 25
}]

let newList = excludeBy(users1, users2, 'name');
console.log(newList);
