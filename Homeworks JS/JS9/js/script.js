/*Реализовать функцию, которая будет считать возраст пользователя и его знак зодиака.

Технические требования:

При запуске программы - в диалоговом окне спросить дату рождения пользователя (текст в формате dd.mm.yyyy)
Вывести в модальном окне сообщение: Вам ? лет! - Вместо ? подставить возраст пользователя.
Вывести в отдельном модальном окне знак зодиака пользователя в формате: Ваш знак зодиака: ?.


Не обязательное задание продвинутой сложности:

Выводить также в год какого животного по китайскому календарю родился пользователь.*/

function getAgeZodiak(elem = prompt('Enter you birthday as dd.mm.yyyy', '')) {
    function replaceDayMonth(str) {
        let arr = str.split('.');
        arr[3] = arr[0];
        arr[0] = arr[1];
        arr[1] = arr[3];
        arr.length = 3;
        str = arr.join('.');
        return str;
    }

    function getAge(birth) {
        let nowDate = new Date();
        let userAge = nowDate.getFullYear() - birth.getFullYear();
        if (birth.getMonth() > nowDate.getMonth() || (birth.getMonth() == nowDate.getMonth() && birth.getDate() > nowDate.getDate())) {
            alert('Вам ' + (userAge - 1) + ' лет');
        } else {
            alert('Вам ' + userAge + ' лет');
        }
        return true;
    }


    function getZodiak(day, mon) {
        let signs = [
            {
                name: 'козерог',
                d: 20
        },
            {
                name: 'водолей',
                d: 20
        },
            {
                name: 'рыбы',
                d: 20
        },
            {
                name: 'овен',
                d: 20
        },
            {
                name: 'телец',
                d: 20
        },
            {
                name: 'близнецы',
                d: 21
        },
            {
                name: 'рак',
                d: 22
        },
            {
                name: 'лев',
                d: 23
        },
            {
                name: 'дева',
                d: 23
        },
            {
                name: 'весы',
                d: 23
        },
            {
                name: 'скорпион',
                d: 22
        },
            {
                name: 'стрелец',
                d: 21
        },
            {
                name: 'козерог',
                d: 20
        }
        ];

        if (day <= signs[mon].d) {
            alert('Ваш знак зодиака: ' + signs[mon].name);
        } else {
            alert('Ваш знак зодиака: ' + signs[mon + 1].name);
        }

    }

    function getChinaYear(year) {
        let chYears = [
            {
                name: 'крыса',
                y: 1900
        },
            {
                name: 'бык',
                y: 1901
        },
            {
                name: 'тигр',
                y: 1902
        },
            {
                name: 'кролик',
                y: 1903
        },
            {
                name: 'дракон',
                y: 1904
        },
            {
                name: 'змея',
                y: 1905
        },
            {
                name: 'лошадь',
                y: 1906
        },
            {
                name: 'коза',
                y: 1907
        },
            {
                name: 'обезьяна',
                y: 1908
        },
            {
                name: 'петух',
                y: 1909
        },
            {
                name: 'собака',
                y: 1910
        },
            {
                name: 'свинья',
                y: 1911
        },
        ];
        chYears.forEach(function (item) {
            if (year % 12 === item.y % 12) {
                alert('Вы родились в год: ' + item.name);
            }
        })

    }

    let birthday = new Date(replaceDayMonth(elem));
    getAge(birthday);
    let month = birthday.getMonth();
    let bday = birthday.getDate();
    let bYear = birthday.getFullYear();    
    getZodiak(bday, month);
    getChinaYear (bYear);

}
getAgeZodiak();
