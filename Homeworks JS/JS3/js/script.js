/*Реализовать функцию подсчета факториала числа.

Технические требования:

Написать функцию подсчета факториала числа.
Считать с помощью модального окна браузера число, которое введет пользователь.
С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
Использовать синтаксис ES6 для работы с перемеными и функциями.*/

function factorialCicle(num)
{
    
    if (num < 0) {
        return false;
    }
    
    else if (num == 0) {
        return 1;
    }
    
    let tmp = num;
    while (--num > 1) {
        tmp *= num;
    }
    return tmp;
}

function factorialRecur(number)
{
    
    if (number < 0) {
        return false;
    }
    
    else if (number == 0) {
        return 1;
    }
    
    else {
        return (number * factorialRecur(number - 1));
    }
}
let userNumber = Number(prompt('Enter the number', ''));
let result = factorialRecur(userNumber);
alert ('The factorial of ' + userNumber + ' is ' + result);