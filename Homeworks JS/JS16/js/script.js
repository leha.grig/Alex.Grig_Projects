/*Реализовать возможность смены цветовой темы сайта пользователем.

Технические требования:

Взять любое готовое домашнее задание по HTML/CSS.
Добавить на макете кнопку "Сменить тему".
При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
Выбранная тема должна сохраняться и после перезагрузки страницы
#### Литература:
- [LocalStorage на пальцах](https://tproger.ru/articles/localstorage/)*/


greyscaleBtn.addEventListener('click', () => {
    let isGreyscale = localStorage.getItem('greyscale');

    if (isGreyscale === null || isGreyscale == 'false') {
        localStorage.setItem('greyscale', 'true');
        main.classList.add('greyscale');
    }
    else {
        localStorage.setItem('greyscale', 'false');
        main.classList.remove('greyscale');
    }
});

let greyscale = localStorage.getItem('greyscale');

if (greyscale == 'true') {
    main.classList.add('greyscale');
}
else {
    main.classList.remove('greyscale');
}

