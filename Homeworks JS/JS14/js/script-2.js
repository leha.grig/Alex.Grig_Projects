/*
Сделать Javascript калькулятор, используя готовую [верстку](calculator.zip).

    #### Технические требования:
    - В архиве дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
- При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
- При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
- Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
- При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `mem` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.

    #### Не обязательное задание продвинутой сложности:
    - Все клавиши калькулятора должны также нажиматься с клавиатуры. Нажатие `Enter` будет развнозначно нажатию `=`

#### Литература:
- [Поиск DOM элементов](https://learn.javascript.ru/searching-elements-dom)
- [Свойства узлов: тип, тег и содержимое](http://learn.javascript.ru/basic-dom-node-properties)
- [Введение в браузерные события](https://learn.javascript.ru/introduction-browser-events)
*/

// универсальная функция вывода значения клавиши клавиатуры. event.type должен быть keypress (не работает для Esc, Shift, Ctrl...)
function getChar(event) {
    if (event.which == null) { // IE
        if (event.keyCode < 32) return null; // спец. символ
        return String.fromCharCode(event.keyCode)
    }
    if (event.which != 0 && event.charCode != 0) { // все кроме IE
        if (event.which < 32) return null; // спец. символ
        return String.fromCharCode(event.which); // остальные
    }
    return null; // спец. символ
}

//математические операции, вывод на экран результата
function calculate() {
    if (sum) {
        display.value = num1 + num2;
        sum = false;
    }
    if (diff) {
        display.value = num1 - num2;
        diff = false;
    }
    if (mult) {
        display.value = num1 * num2;
        mult = false;
    }
    if (divide) {
        display.value = num1 / num2;
        divide = false;
    }
    return true;
}

const keys = document.getElementById('keys');
const display = document.getElementById('display');
const enterKey = document.getElementById('enterKey');
const escKey = document.getElementById('escKey');
const divM = document.getElementsByClassName('memory')[0];

// const eventClick = new Event("click", {bubbles: true, cancelable: false});
// в данном случае в новом событии eventClick нет необходимости, т.к. полностью подходит существующее событие .click()
// Но и eventClick можно было бы адекватно использовать вместо .click() (закоментированный пример в обработчике ниже)

let operator = false;
let num1 = 0;
let num2 = 0;
let sum = false;
let diff = false;
let mult = false;
let divide = false;
let mrc = false;
let mem = 0;


display.value = 0;


// блок подключения клавиатуры, запускает клик-событие на кнопках калькулятора при нажатии соответствующих клавишь клавиатуры
addEventListener('keypress', function (event) {
    let chr = getChar(event);
    let targetKey = null;
    let buttons = document.getElementsByClassName('button');
    for (let i = 0; i < buttons.length; i++) {
        if (chr === buttons[i].value) {
            targetKey = buttons[i];
            targetKey.click();
//          targetKey.dispatchEvent(eventClick); // альтернатива .click() со своим созданным событием

        }
    }
});
addEventListener('keydown', function (event) {
    if (event.keyCode === 13) {
        event.preventDefault(); // необходимо для отмены дефолтного поведения при нажатии (keydown) Enter для инпутов (вводит еще раз последние значение))
        enterKey.click();
    }
    if (event.keyCode === 27) {
        event.preventDefault();
        escKey.click();
    }
    return true;
});

// блок управления калькулятором
keys.addEventListener('click', function (e) {
    let target = e.target;
    if (!isNaN(+target.value)) {
        if (operator || display.value === "0") {
            display.value = '';
            operator = false;
        }
        display.value += target.value;
    }
    if (target.value === '.' && display.value.indexOf('.') < 0 && !operator) {
        display.value += target.value;
    }
    if (target.classList.contains('pink') || target.value === '=') {
        operator = true;
        if (!sum && !diff && !mult && !divide && target.value !== '=') {
            num1 = Number(display.value);
        }
        else {
            num2 = Number(display.value);
            calculate();
            num1 = Number(display.value);
            e.preventDefault();
        }
        if (target.value === '+') {
            sum = true;
        }
        if (target.value === '-') {
            diff = true;
        }
        if (target.value === '*') {
            mult = true;
        }
        if (target.value === '/') {
            divide = true;
        }
    }
    if (target.value === 'C') {
        num1 = 0;
        num2 = 0;
        sum = diff = mult = divide = false;
        display.value = 0;
        operator = false;
    }
    if (target.value === 'm+' || target.value === 'm-') {
        divM.classList.add('active');
        operator = true;
        if (target.value === 'm+') {
            mem += Number(display.value);
        }
        if (target.value === 'm-') {
            mem -= Number(display.value);
        }
    }
    if (target.value !== 'mrc') {
        mrc = false;
    }
    if (target.value === 'mrc' && mem !== 0) {
        operator = true;
        if (!mrc) {
            display.value = mem;
            mrc = true;
        }
        else {
            divM.classList.remove('active');
            mem = 0;
            mrc = false;
        }
    }
    return true;
});
