/*Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем с помощью модальных окон браузера - alert, prompt, confirm.

Технические требования:

Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome, + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
Если возраст больше 22 лет - показать на экране сообщение: Welcome, + имя пользователя.
Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


Не обязательное задание продвинутой сложности:

После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).*/


// с помощью функции-конструктора:

function alowAccess() {
    this.name = '';
    this.age = 1;
    this.access = false;
    self = this;

    function requestNameAge(nameDflt, ageDflt) {
        self.name = prompt('Enter your name', nameDflt);

        self.age = Number(prompt('Enter your age', ageDflt));
        return true;
    };

    let userData = requestNameAge();

    while (isNaN(this.age) || this.name == null || this.name == '') {
        alert('Enter your name and age in correct format');
        userData = requestNameAge(this.name, this.age);

    }
    if (this.age < 18) {
        alert('You are not allowed to visit this website.');
    } else if (this.age < 23) {
        let agreementToVisit = confirm('Are you sure you want to continue?');
        if (agreementToVisit) {
            alert('Welcome, ' + this.name);
            this.access = true;
        } else {
            alert('You are not allowed to visit this website.')
        }

    } else {
        alert('Welcome, ' + this.name);
        this.access = true;
    }
    return false;
}

let newUser = new alowAccess();


// с помощью самовызывающейся функции:
/*

(function alowAccess() {
    
    function requestNameAge(nameDflt, ageDflt) {
        let name = prompt('Enter your name', nameDflt);

        let age = Number(prompt('Enter your age', ageDflt));
        return [name, age];
    }

    let userData = requestNameAge();
    let userName = userData[0];
    let userAge = userData[1];

    while (isNaN(userAge) || userName == null || userName == '') {
        alert('Enter your name and age in correct format');
        userData = requestNameAge(userName, userAge);
        userName = userData[0];
        userAge = userData[1];
    }
    if (userAge < 18) {
        alert('You are not allowed to visit this website.');
    } else if (userAge < 23) {
        let agreementToVisit = confirm('Are you sure you want to continue?');
        if (agreementToVisit) {
            alert('Welcome, ' + userName);
        } else {
            alert('You are not allowed to visit this website.')
        }

    } else {
        alert('Welcome, ' + userName);
    }
    return false;
})();
*/
