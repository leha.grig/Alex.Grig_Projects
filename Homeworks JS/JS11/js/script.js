/*Реализовать функцию, которая будет добавлять список, введенный пользователем, на страницу.

Технические требования:

При запуске программы спросить у пользователя в модальном окне число пунктов, которые будут в списке.
После введения пользователем числа, вывести модальное окно еще нужное количество раз (такое количество раз, какое число ввел пользователь в первом модальном окне), спрашивая у пользователя содержимое пунктов списка (пользователь может ввести любой текст).
После ввода последнего значения, вывести на странице список, введенный пользователем с помощью модальных окон.
Через 10 секунд очистить страницу.
Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка пеерд выведением его на страницу.


Не обязательное задание продвинутой сложности:

Показывать таймер обратного отсчета (только секунды) перед очищением страницы.*/



function addNewList(liNumber = prompt('Enter the number of items in the list', '')) {
    // проверка корректности номера
    if (isNaN(Number(liNumber)) || Number(liNumber) <= 0) {
        alert('The entered number is incorrect');
        return false;
    }

    // создание массива из строк для пунктов
    let listArr = [];
    for (i = 0; i < Number(liNumber); i++) {
        listArr.push(prompt('Enter the text for the item number ' + (i + 1), ''));
    }

   /* // преобразование содержимого listArr и вывод без шаблонных строк:
   
   // преобразование массива в массив элементов li
    let liArr = listArr.map(function (item) {
        let li = document.createElement('li');
        li.innerHTML = item;
        return li;
    });

    // создание элемента ul и наполнение его элементами li из массива
    let ul = document.createElement('ul');
    liArr.forEach(function (item) {
        ul.appendChild(item);
    })

    //     вывод ul на страницу
    document.body.appendChild(ul);
    
    */


    //  альтернатива - вывод с помощью шаблонных строк:
    
    document.body.innerHTML = `<ul> ${
        listArr.map(function (item) {
        let li = `<li> ${item} </li>`;
        return li;
    }).join(" ")} </ul>`;

    // таймер и вывод его на страницу
    let p = document.createElement('p');
    let count = 10;
    p.innerHTML = count;
    document.body.appendChild(p);

    let timer = setInterval(function () {
        count--;
        p.innerHTML = count;
    }, 1000);

    // очистка страницы после 10 с
    let timeOut = setTimeout(function () {
        document.body.innerHTML = '';
    }, 10000);
}

addNewList();
